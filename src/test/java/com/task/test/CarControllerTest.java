package com.task.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.task.controller.CarController;
import com.task.repository.CarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.task.domain.Cars;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CarControllerTest {

    @InjectMocks
    private CarController carController;

    @Mock
    private CarRepository carRepository;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetCarById() {
        Cars car = new Cars();
        car.setId(1);
        when(carRepository.getCarsById(1)).thenReturn(car);
        Cars cars = carController.searchCars(1);
        verify(carRepository).getCarsById(1);
        assertEquals(1, cars.getId());
    }
}
