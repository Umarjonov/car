package com.task.controller;

import com.task.domain.Cars;
import com.task.repository.CarRepository;
import com.task.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {

    @Autowired
    private CarService carService;
    @Autowired
    private CarRepository carRepository;

    @GetMapping("/")
    public List<Cars> viewHomePage(Model model){
        return carRepository.findAll();
    }

    @PostMapping("/add")
    public Cars addNewCar(@RequestBody Cars car){
       return carService.save(car);
    }

    @GetMapping("/cars/{id}")
    public Cars searchCars(@PathVariable long id){
        return carRepository.getCarsById(id);
    }

}
