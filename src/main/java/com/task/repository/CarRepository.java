package com.task.repository;

import com.task.domain.Cars;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CarRepository extends JpaRepository<Cars, Long> {

     Cars getCarsById(long id);
    
}
