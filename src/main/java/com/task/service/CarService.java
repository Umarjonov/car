package com.task.service;

import com.task.domain.Cars;
import com.task.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    @Transactional(readOnly = true, propagation =   Propagation.SUPPORTS)
    public List<Cars> carsList(){
        return carRepository.findAll();
    }

    @Transactional
    public Cars save(Cars car){
        carRepository.save(car);
        return car;
    }

    @Transactional(readOnly = true, propagation =   Propagation.SUPPORTS)
    public Cars findByCarId(long id){
        Cars cars = carRepository.getCarsById(id);
        return cars;
    }
}
